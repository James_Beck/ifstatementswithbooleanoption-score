﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IfStatementsWithBooleanOption_Score
{
    class Program
    {
        static void Main(string[] args)
        {
            ///Where the variables are listed
            var user = "0";
            var rules1 = "The rules are simple, the console is going to ask you a true or false question.";
            var rules2 = "If you get the question correct you get a word which is part of a larger question.";
            var rules3 = "Your goal is to try and get enough true or false questions right to answer the final question";
            int score = 0;
            var answer1 = "What";
            var answer2 = "animal";
            var answer3 = "when";
            var answer4 = "kissed";
            var answer5 = "turns";
            var answer6 = "into";
            var answer7 = "a";
            var answer8 = "prince?";
            var answerwrong = "____";
            var storeanswer1 = "0";
            var storeanswer2 = "0";
            var storeanswer3 = "0";
            var storeanswer4 = "0";
            var storeanswer5 = "0";
            var storeanswer6 = "0";
            var storeanswer7 = "0";
            var storeanswer8 = "0";
            var finalanswer = "0";
            List<string> answers = new List<string>();

            ///Interacting with the user by asking for a name and explaining the rules
            Console.WriteLine("What's your name?");
            user = Console.ReadLine();
            Console.WriteLine($"Welcome {user}, you are here to play a little game");
            Console.WriteLine(rules1);
            Console.WriteLine(rules2);
            Console.WriteLine(rules3);
            Console.WriteLine();
            Console.WriteLine("Make sure you answers are exactly as written in the apostrophes: 'true' or 'false', otherwise they are considered incorrect");
            Console.WriteLine();
            Console.WriteLine("When you have an understanding of the rules, press any key to continue");
            Console.ReadKey();
            Console.Clear();

            ///Game start
            Console.WriteLine($"Alright {user}, first question: true or false; caesium is the largest atom?");
            storeanswer1 = Console.ReadLine();
            if (storeanswer1 == "true")
            {
                Console.WriteLine(answer1);
                score++;
                answers.Add(answer1);
            }
            else
            {
                Console.WriteLine(answerwrong);
                answers.Add(answerwrong);
            }
            Console.WriteLine();
            Console.WriteLine("Second question: true or false; the elephant is the tallest animal?");
            storeanswer2 = Console.ReadLine();
            if (storeanswer2 == "false")
            {
                Console.WriteLine(answer2);
                score++;
                answers.Add(answer2);
            }
            else
            {
                Console.WriteLine(answerwrong);
                answers.Add(answerwrong);
            }
            Console.WriteLine();
            Console.WriteLine("Third question: true or false; Leonardo Da Vinci was born in the 1500's?");
            storeanswer3 = Console.ReadLine();
            if (storeanswer3 == "false")
            {
                Console.WriteLine(answer3);
                score++;
                answers.Add(answer3);
            }
            else
            {
                Console.WriteLine(answerwrong);
                answers.Add(answerwrong);
            }
            Console.WriteLine();
            Console.WriteLine("Fourth question: true or false; 75! is greater than a googol?");
            storeanswer4 = Console.ReadLine();
            if (storeanswer4 == "true")
            {
                Console.WriteLine(answer4);
                score++;
                answers.Add(answer4);
            }
            else
            {
                Console.WriteLine(answerwrong);
                answers.Add(answerwrong);
            }
            Console.WriteLine();
            Console.WriteLine("Fifth question: true or false; the Whale Shark is the largest fish alive?");
            storeanswer5 = Console.ReadLine();
            if (storeanswer5 == "true")
            {
                Console.WriteLine(answer5);
                score++;
                answers.Add(answer5);
            }
            else
            {
                Console.WriteLine(answerwrong);
                answers.Add(answerwrong);
            }
            Console.WriteLine();
            Console.WriteLine("Sixth question: true or false; the golden fleece was collected by Jason and argonauts to make Jason King?");
            storeanswer6 = Console.ReadLine();
            if (storeanswer6 == "true")
            {
                Console.WriteLine(answer6);
                score++;
                answers.Add(answer6);
            }
            else
            {
                Console.WriteLine(answerwrong);
                answers.Add(answerwrong);
            }
            Console.WriteLine();
            Console.WriteLine("Seventh question: true or false; in 1066AD a famous battle occurred known as the battle of tastings?");
            storeanswer7 = Console.ReadLine();
            if (storeanswer7 == "false")
            {
                Console.WriteLine(answer7);
                score++;
                answers.Add(answer7);
            }
            else
            {
                Console.WriteLine(answerwrong);
                answers.Add(answerwrong);
            }
            Console.WriteLine();
            Console.WriteLine("Final question: true or false; Joey Chestnut has won 12 'Nathan's Hotdog' eating competitions?");
            storeanswer8 = Console.ReadLine();
            if (storeanswer8 == "false")
            {
                Console.WriteLine(answer8);
                score++;
                answers.Add(answer8);
            }
            else
            {
                Console.WriteLine(answerwrong);
                answers.Add(answerwrong);
            }
            Console.Clear();
            //Way to communicate how well someone did in the game with a special message for certain scores
            Console.WriteLine($"{user} your managed to get {score}/8 true or false questions right.");
            if (score == 8)
            {
                Console.WriteLine("CONGRATULATIONS!");
            }
            else if (score >= 5 && score != 8)
            {
                Console.WriteLine("Not a bad attempt.");
            }
            else
            {
                Console.WriteLine("Maybe try again.");
            }
            Console.WriteLine();
            //Final question
            Console.WriteLine($"Well {user}, here's the question");
            foreach(string answer in answers)
            {
                Console.WriteLine(answer);
            }
            Console.WriteLine();
            finalanswer = Console.ReadLine();
            //the final if statement to determine a win or loss of the game
            if (finalanswer == "frog" || finalanswer == "a frog")
            {
                Console.WriteLine($"{user}, congratulations, you've just won the game!");
            }
            else
            {
                Console.WriteLine($"I'm sorry {user} but you have lost the game.");
            }
        }
    }
}
